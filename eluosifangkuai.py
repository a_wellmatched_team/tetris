import pygame, sys, random, time
 
def new_draw():
    screen.fill(white)
 
    for i in range(1, 21):
        for j in range(10):
            bolck = background[i][j]
            if bolck:
                pygame.draw.rect(screen,darkblue, (j * 25 + 1, 500 - i * 25 + 1, 23, 23))
 
    x, y = centre
    for i, j in active:
        i += x
        j += y
        pygame.draw.rect(screen, green, (j * 25 + 1, 500 - i * 25 + 1, 23, 23))
 
    pygame.display.update()#更新
 
 
def move_LR(n):
    """n=-1代表向左，n=1代表向右"""
    x, y = centre
    y += n
    for i, j in active:
        i += x
        j += y
        if j < 0 or j > 9 or background[i][j]:
            break
    else:
        centre.clear()
        centre.extend([x, y])
    
    #通过backgound[i][j]判断新位置是否被占用
    #通过j < 0 or j > 9 判断是否超出宽度范围
    #1.如果新位置被占用或者超出宽度范围，则break 跳出for循环且不执行else语句
    #2.如果新位置未被占用 或者没超出宽度范围  则更新centre
 
def rotate():
    x, y = centre
    l = [(-j, i) for i, j in active]
    for i, j in l:
        i += x
        j += y
        if j < 0 or j > 9 or background[i][j]:
           
            break
    else:
        active.clear()
        active.extend(l)
    #通过backgound[i][j]判断新位置是否被占用
    #通过j < 0 or j > 9 判断是否超出宽度范围
    #1.如果新位置被占用或者超出宽度范围，则break 跳出for循环且不执行else语句
    #2.如果新位置未被占用 或者没超出宽度范围  则更新l列表
 
 
def move_down():
    x, y = centre
    x -= 1
    for i, j in active:
        i += x
        j += y
        if background[i][j]:
            break
    else:
        centre.clear()
        centre.extend([x, y])
        return
    #通过backgound[i][j]判断新位置是否被占用
    #1.如果新位置被占用 则不执行else语句  而是继续向下执行
    #2.如果新位置未被占用 执行else的语句  并通过return直接结束
    x, y = centre
    for i, j in active:
        background[x + i][y + j] = 1
 
    l = []
    for i in range(1, 20):
        if 0 not in background[i]:
            l.append(i)
    # l装 行号，鉴于删去后，部分索引变化，对其降序排列，倒着删除
    l.sort(reverse=True)
 
    for i in l:
        background.pop(i)
        background.append([0 for j in range(10)])
        # 随删随补
 
    score[0] += len(l)*10
    pygame.display.set_caption("分数：%d" % (score[0]))
 
    active.clear()
    active.extend(list(random.choice(all_block)))
    # all_block保存7种形状的信息，手打出来的
    centre.clear()
    centre.extend([20, 4])
 
    x, y = centre
    for i, j in active:
        i += x
        j += y
        if background[i][j]:
            break
    else:
        return
    alive.append(1)
 
 
pygame.init()
screen = pygame.display.set_mode((250, 500))
pygame.display.set_caption("俄罗斯方块")
fclock = pygame.time.Clock()
 
all_block = (((0, 0), (0, -1), (0, 1), (0, 2)),
             ((0, 0), (0, 1), (-1, 0), (-1, 1)),
             ((0, 0), (0, -1), (-1, 0), (-1, 1)),
             ((0, 0), (0, 1), (-1, -1), (-1, 0)),
             ((0, 0), (0, 1), (1, 0), (0, -1)),
             ((0, 0), (1, 0), (-1, 0), (1, -1)),
             ((0, 0), (1, 0), (-1, 0), (1, 1)))
background = [[0 for i in range(10)] for j in range(24)]
background[0] = [1 for i in range(10)]
active = list(random.choice(all_block))
centre = [20, 4]
score = [0]
 
 
green = 100,255,100 
white = 255, 255, 255
darkblue=0,0,139

times = 0
alive = []
a=0
press = False
while True:
    for event in pygame.event.get():
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                move_LR(-1)
            elif event.key == pygame.K_RIGHT:
                move_LR(1)
            elif event.key == pygame.K_UP:
                rotate()
            elif event.key == pygame.K_DOWN:
                press = True
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_DOWN:
                press = False
    if press:
        times += 10
 
    if times >= 50:
        move_down()
        times = 0
    else:
        times += 1
        a+=1
        if a>=10000 and a%50==0:
            move_down()
            move_down()
            
    if alive:
        pygame.display.set_caption("最终分数:%d" % (score[0]))
        time.sleep(3)
        break
    new_draw()
   
    fclock.tick(100)













    
 
 

 
