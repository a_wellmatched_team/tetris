import pygame, sys, random, time
def move_LR(n):
    """n=-1代表向左，n=1代表向右"""
    x, y = centre
    y += n
    for i, j in active:
        i += x
        j += y
        if j < 0 or j > 9 or background[i][j]:
            break
    else:
        centre.clear()
        centre.extend([x, y])
    
    #通过backgound[i][j]判断新位置是否被占用
    #通过j < 0 or j > 9 判断是否超出宽度范围
    #1.如果新位置被占用或者超出宽度范围，则break 跳出for循环且不执行else语句
    #2.如果新位置未被占用 或者没超出宽度范围  则更新centre
 
