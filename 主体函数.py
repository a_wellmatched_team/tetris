import pygame, sys, random, time
pygame.init()
screen = pygame.display.set_mode((250, 500))
pygame.display.set_caption("俄罗斯方块")
fclock = pygame.time.Clock()
 
all_block = (((0, 0), (0, -1), (0, 1), (0, 2)),
             ((0, 0), (0, 1), (-1, 0), (-1, 1)),
             ((0, 0), (0, -1), (-1, 0), (-1, 1)),
             ((0, 0), (0, 1), (-1, -1), (-1, 0)),
             ((0, 0), (0, 1), (1, 0), (0, -1)),
             ((0, 0), (1, 0), (-1, 0), (1, -1)),
             ((0, 0), (1, 0), (-1, 0), (1, 1)))
background = [[0 for i in range(10)] for j in range(24)]
background[0] = [1 for i in range(10)]
active = list(random.choice(all_block))
centre = [20, 4]
score = [0]
 
 
green = 100,255,100 
white = 255, 255, 255
darkblue=0,0,139

times = 0
alive = []
a=0
press = False
while True:
    for event in pygame.event.get():
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                move_LR(-1)
            elif event.key == pygame.K_RIGHT:
                move_LR(1)
            elif event.key == pygame.K_UP:
                rotate()
            elif event.key == pygame.K_DOWN:
                press = True
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_DOWN:
                press = False
    if press:
        times += 10
 
    if times >= 50:
        move_down()
        times = 0
    else:
        times += 1
        a+=1
        if a>=10000 and a%50==0:
            move_down()
            move_down()
            
    if alive:
        pygame.display.set_caption("最终分数:%d" % (score[0]))
        time.sleep(3)
        break
    new_draw()
   
    fclock.tick(100)

