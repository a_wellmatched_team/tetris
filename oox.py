#下落函数  
def move_down():
    x, y = centre
    x -= 1
    for i, j in active:
        i += x
        j += y
        if background[i][j]:#如果此时if条件成立，执行break之后直接跳过else语句向下执行
            break
    else:
        centre.clear()
        centre.extend([x, y])
        return
    # 如果新位置未被占用 通过return结束
    # 如果新位置被占用则继续  向下执行
    x, y = centre
    for i, j in active:
        background[x + i][y + j] = 1#现在这个方块就已经停止下落，根据这个图形中的四个小正方形的左上角的坐标i,j.将background[x + i][y + j]定义为1,此时这个图形颜色是蓝色
 
    l = []#定义一个空列表，用来存储后面判断的要删除的列
    for i in range(1, 20):
        if 0 not in background[i]:
            l.append(i)#如果background[i]中所有值都不为零，则将i加入到l[]中
    l.sort(reverse=True)
     # l装行号，鉴于删去后，部分索引变化，对其降序排列，倒着删除.
 
    for i in l:
        background.pop(i)#将background列表中的第i项元素取出并删除该元素
        background.append([0 for j in range(10)])
        # 随删随补
 
    score[0] += len(l)#分数将会加上这个l列表的长度
    pygame.display.set_caption("分数：%d" % (score[0]))#在窗口的标题上会打印出分数等于多少
 
    active.clear()#将active清空
    active.extend(list(random.choice(all_block)))#再次从all_block中随机取出一组二元组
    centre.clear()#将centre清空
    centre.extend([20, 4])#将centre还原为初始值
 
    x, y = centre
    for i, j in active:
        i += x
        j += y
        if background[i][j]:
            break
    else: 
        return
    alive.append(1)
